(module srfi-145 (assume)

(import scheme)
(import (chicken base))
(import (chicken platform))

(register-feature! 'srfi-145)

(define-syntax assume
  (syntax-rules ()
    ((_ obj message ...)
     (assert obj message ...))))

)
