(import scheme)
(import test)
(import (srfi 145))

(test 123 (assume 123 "True values return the object unchanged"))
(test-error (assume #f "False ones raise an error"))

(test-exit)
