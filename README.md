## About

Port of [SRFI-145](https://srfi.schemers.org/srfi-145/srfi-145.html)
(Assumptions) to CHICKEN.

## Docs

See [its wiki page].

[its wiki page]: https://wiki.call-cc.org/eggref/5/srfi-145
